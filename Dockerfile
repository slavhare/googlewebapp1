# we are extending everything from tomcat:8.0 image ...
FROM tomcat:8.0.51-jre8-alpine
MAINTAINER slavhare@ptc.com
RUN cd /usr/local/
RUN rm -rf /usr/local/tomcat/webapps/*
# COPY path-to-your-application-war path-to-webapps-in-docker-tomcat
COPY ./target/*.war /usr/local/tomcat/webapps/ROOT.war
CMD ["catalina.sh","run"]
